;;; Dungeon Master --- RPG Adventure Generator
;;; Copyright © 2020 Javier Sancho <jsf@jsancho.org>
;;;
;;; Dungeon Master is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Dungeon Master is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Dungeon Master. If not, see <http://www.gnu.org/licenses/>.


(define-module (dungeon-master dialogue)
  #:use-module (ice-9 optargs)
  #:export (run-dialogue))


(define* (run-dialogue dialogue #:optional initial-scene)
  (let ((scene (if initial-scene
                   (assoc-ref dialogue initial-scene)
                   (cdar dialogue))))
    (let-keywords scene #t (text)
                  (display text)
                  (newline))))
