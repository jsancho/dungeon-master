;;; Dungeon Master --- RPG Adventure Generator
;;; Copyright © 2019 Javier Sancho <jsf@jsancho.org>
;;;
;;; Dungeon Master is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Dungeon Master is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Dungeon Master. If not, see <http://www.gnu.org/licenses/>.


(define-module (dungeon-master geom)
  #:use-module (dungeon-master geom point)
  #:export (angle-sign))


(define (angle-sign center a b)
  "Return the sign of (- (atan (- a center))
                         (atan (- b center)))"
  (let ((x1 (- (point-x a) (point-x center)))
        (y1 (- (point-y a) (point-y center)))
        (x2 (- (point-x b) (point-x center)))
        (y2 (- (point-y b) (point-y center))))
    (cond ((and (>= x1 0) (< x2 0))
             1)
          ((and (>= x2 0) (< x1 0))
             -1)
          ((and (= x1 0) (= x2 0))
           (if (> y2 y1) 1 -1))
          (else
           (if (> (- (* x2 y1) (* x1 y2)) 0) 1 -1)))))
