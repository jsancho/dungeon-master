;;; Dungeon Master --- RPG Adventure Generator
;;; Copyright © 2019 Javier Sancho <jsf@jsancho.org>
;;;
;;; Dungeon Master is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Dungeon Master is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Dungeon Master. If not, see <http://www.gnu.org/licenses/>.


(define-module (dungeon-master geom point)
  #:use-module (srfi srfi-9)
  #:export (make-point
            point?
            point-x
            point-y
	    points-distance
            sum-points
            scale-point))

(define-record-type <point>
  (make-point x y)
  point?
  (x point-x)
  (y point-y))

(define (points-distance p1 p2)
  (abs
   (sqrt (+ (expt (- (point-x p1) (point-x p2)) 2)
            (expt (- (point-y p1) (point-y p2)) 2)))))

(define (sum-points . points-to-sum)
  (let loop ((points points-to-sum)
             (x 0)
             (y 0))
    (cond ((null? points)
           (make-point x y))
          (else
           (loop (cdr points)
                 (+ x (point-x (car points)))
                 (+ y (point-y (car points))))))))

(define (scale-point point scale)
  (make-point
   (* (point-x point) scale)
   (* (point-y point) scale)))
