;;; Dungeon Master --- RPG Adventure Generator
;;; Copyright © 2019 Javier Sancho <jsf@jsancho.org>
;;;
;;; Dungeon Master is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Dungeon Master is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Dungeon Master. If not, see <http://www.gnu.org/licenses/>.


(define-module (dungeon-master generators town)
  #:use-module (dungeon-master geom voronoi)
  #:use-module (dungeon-master geom point)
  #:export (generate))

(define (random-bool)
  (= (random 2) 1))

(define pi 3.141592654)
(define relax-steps 3)

(define (generate patches)
  "City generator from https://github.com/watabou/TownGeneratorOS/blob/master/Source/com/watabou/towngenerator/building/Model.hx"
  (set! *random-state* (random-state-from-platform))
  (when (= patches -1) (set! patches 15))
  (build-patches patches))

(define (build-patches n-patches)
  (define* (get-points n seed #:optional (l '()))
    (cond ((>= n 0)
           (let* ((a (+ seed (* (sqrt n) 5)))
                  (r (if (= n 0)
                         0
                         (+ 10 (* n (+ 2 (random:exp))))))
                  (point (make-point
                          (* (cos a) r)
                          (* (sin a) r))))
             (get-points (- n 1) seed (cons point l))))
          (else
           l)))

  (define (relax voronoi n step)
    "Relaxing central wards"
    (cond ((> step 0)
           (let* ((voronoi-points (voronoi-mesh-points voronoi))
                  (n-points (length voronoi-points))
                  (to-relax (cons (list-ref voronoi-points (- n-points n-patches))
                                  (list-tail voronoi-points (- n-points 3)))))
             (relax (voronoi-mesh-relax voronoi to-relax) n (- step 1))))
          (else
           voronoi)))

  (let* ((sa (* (random:exp) 2 pi))
         (points (get-points (* 8 n-patches) sa))
         (voronoi (relax (make-voronoi-mesh points) n-patches relax-steps)))
    "end"))
