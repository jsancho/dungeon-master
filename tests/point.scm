;;; Dungeon Master --- RPG Adventure Generator
;;; Copyright © 2019 Javier Sancho <jsf@jsancho.org>
;;;
;;; Dungeon Master is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Dungeon Master is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Dungeon Master. If not, see <http://www.gnu.org/licenses/>.


(define-module (test-point)
  #:use-module (srfi srfi-64)
  #:use-module (dungeon-master geom point))

(test-begin "point")

(test-equal
 "distance"
 (points-distance
  (make-point 0 0)
  (make-point 0 -10))
 10)

(test-equal
 "sum points"
 (sum-points
  (make-point 1 2)
  (make-point 3 4)
  (make-point 5 6))
 (make-point 9 12))

(test-equal
 "scale point"
 (scale-point (make-point 2 5) 2.5)
 (make-point 5.0 12.5))

(test-end)

(exit (zero? (test-runner-fail-count (test-runner-current))))
