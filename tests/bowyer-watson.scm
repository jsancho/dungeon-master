;;; Dungeon Master --- RPG Adventure Generator
;;; Copyright © 2019 Javier Sancho <jsf@jsancho.org>
;;;
;;; Dungeon Master is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Dungeon Master is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Dungeon Master. If not, see <http://www.gnu.org/licenses/>.


(define-module (test-bowyer-watson)
  #:use-module (ice-9 receive)
  #:use-module (srfi srfi-64)
  #:use-module (dungeon-master geom bowyer-watson)
  #:use-module (dungeon-master geom point))

(test-begin "bowyer-watson")

(receive (triangles points frame)
    (bowyer-watson '())
  (test-equal
   "null triangles"
   (length triangles)
   2)
  (test-equal
   "null points"
   (length points)
   4)
  (test-equal
   "null frame"
   (length frame)
   4))

(define p1 (make-point 0 0))
(define p2 (make-point 0 10))
(define p3 (make-point 10 0))
(define p4 (make-point 10 10))
(define vertices (list p1 p2 p3 p4))

(receive (triangles points frame)
    (bowyer-watson vertices)
  (test-equal
   "simple triangles"
   (length triangles)
   10)
  (test-equal
   "simple points"
   (length points)
   8)
  (test-equal
   "simple frame"
   frame
   (list (make-point -2.5 -2.5)
	 (make-point -2.5 12.5)
	 (make-point 12.5 -2.5)
	 (make-point 12.5 12.5))))

(test-end)

(exit (zero? (test-runner-fail-count (test-runner-current))))
