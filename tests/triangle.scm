;;; Dungeon Master --- RPG Adventure Generator
;;; Copyright © 2019 Javier Sancho <jsf@jsancho.org>
;;;
;;; Dungeon Master is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Dungeon Master is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Dungeon Master. If not, see <http://www.gnu.org/licenses/>.


(define-module (test-triangle)
  #:use-module (srfi srfi-64)
  #:use-module (dungeon-master geom point)
  #:use-module (dungeon-master geom triangle))

(test-begin "triangle")

(define p1 (make-point 0 0))
(define p2 (make-point 0 10))
(define p3 (make-point 10 0))
(define tr (make-triangle p1 p2 p3))

(test-equal
 "circumcenter"
 (triangle-center tr)
 (make-point 5 5))

(test-assert
 "radius"
 (= (triangle-radius tr)
    (points-distance (triangle-center tr) p1)
    (points-distance (triangle-center tr) p2)
    (points-distance (triangle-center tr) p3)))

(test-assert
 "has edge"
 (and (triangle-has-edge tr p1 p2)
      (triangle-has-edge tr p2 p1)
      (triangle-has-edge tr p3 p1)))

(test-end)

(exit (zero? (test-runner-fail-count (test-runner-current))))
