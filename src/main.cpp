/* Dungeon Master --- RPG Adventure Generator
   Copyright © 2019 Javier Sancho <jsf@jsancho.org>

   Dungeon Master is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   Dungeon Master is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Dungeon Master. If not, see <http://www.gnu.org/licenses/>.
*/

#include <irrlicht/irrlicht.h>
#include "modules.h"

using namespace irr;
using namespace core;
using namespace video;
using namespace gui;

int
main ()
{
  load_dmaster_modules ();

  IrrlichtDevice* device =
    createDevice (video::EDT_OPENGL, core::dimension2d<u32> (640, 480));

  if (!device)
    return 1;

  device->setWindowCaption (L"Dungeon Master");

  IVideoDriver* driver = device->getVideoDriver ();
  IGUIEnvironment* guienv = device->getGUIEnvironment ();

  guienv->addStaticText (L"Aqui va el texto", rect<s32> (10, 10, 260, 22));

  while (device->run ())
    {
      driver->beginScene (true, true, SColor(255, 100, 101, 140));
      guienv->drawAll ();
      driver->endScene ();
    }

  device->drop ();
  return 0;
}
