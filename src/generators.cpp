/* Dungeon Master --- RPG Adventure Generator
   Copyright © 2019 Javier Sancho <jsf@jsancho.org>

   Dungeon Master is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   Dungeon Master is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Dungeon Master. If not, see <http://www.gnu.org/licenses/>.
*/

#include <libguile.h>
#include <list>
#include <map>
#include "generators.h"

struct compare_strings
{
  bool operator() (const char* a, const char* b) const
  {
    return strcmp (a, b) < 0;
  }
};

std::map<char*,
         std::list<SceneGenerator*>,
         compare_strings> registered_generators;

SceneGenerator*
register_generator (char* name,
                    char* type,
                    SCM proc)
{
  SceneGenerator* generator = (SceneGenerator*) malloc (sizeof (SceneGenerator));
  generator->name = name;
  generator->type = type;
  generator->proc = proc;

  registered_generators[type].push_back (generator);

  return generator;
}
