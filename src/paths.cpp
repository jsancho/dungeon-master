/* Dungeon Master --- RPG Adventure Generator
   Copyright © 2019 Javier Sancho <jsf@jsancho.org>

   Dungeon Master is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   Dungeon Master is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Dungeon Master. If not, see <http://www.gnu.org/licenses/>.
*/

#include <unistd.h>
#include "paths.h"

std::string
get_exec_path ()
{
  char pBuf[256];
  size_t len = sizeof (pBuf);
  char szTmp[32];

  sprintf (szTmp, "/proc/%d/exe", getpid ());
  int bytes = readlink (szTmp, pBuf, len);
  if (bytes > len - 1)
    bytes = len - 1;
  if (bytes >= 0)
    pBuf[bytes] = '\0';

  std::string exec_path = pBuf;
  return exec_path.substr (0, exec_path.rfind (PATH_DELIM) + 1);
}

std::string
get_home_path ()
{
  return (std::string) getenv ("HOME") + PATH_DELIM + "." + PROGRAM_NAME;
}

std::set<std::string>
get_dmaster_paths () {
  std::set<std::string> paths;
  paths.insert (get_exec_path ());
  paths.insert (DATA_PATH);
  paths.insert (get_home_path ());
  return paths;
}
