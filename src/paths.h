/* Dungeon Master --- RPG Adventure Generator
   Copyright © 2019 Javier Sancho <jsf@jsancho.org>

   Dungeon Master is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   Dungeon Master is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Dungeon Master. If not, see <http://www.gnu.org/licenses/>.
*/

#include <set>
#include <iostream>

#define PATH_DELIM "/"

#ifndef DATA_PATH
#define DATA_PATH ""
#endif

#ifndef PROGRAM_NAME
#define PROGRAM_NAME "dmaster"
#endif

std::set<std::string>
get_dmaster_paths ();
