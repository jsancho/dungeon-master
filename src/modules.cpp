/* Dungeon Master --- RPG Adventure Generator
   Copyright © 2019 Javier Sancho <jsf@jsancho.org>

   Dungeon Master is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   Dungeon Master is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Dungeon Master. If not, see <http://www.gnu.org/licenses/>.
*/

#include <dirent.h>
#include <libguile.h>
#include "generators.h"
#include "modules.h"
#include "paths.h"

SCM
register_scene_generator (SCM name,
                          SCM type,
                          SCM proc)
{
  SceneGenerator* generator =
    register_generator (scm_to_utf8_string (name),
                        scm_to_utf8_string (scm_symbol_to_string (type)),
                        proc);
  printf ("Register: %s (%s)\n", generator->name, generator->type);
  scm_call_1 (generator->proc, scm_from_int (-1));
  return SCM_UNSPECIFIED;
}

void
init_dungeon_master_module (void *unused)
{
  scm_c_define_gsubr ("register-scene-generator", 3, 0, 0, (scm_t_subr) register_scene_generator);
  scm_c_export ("register-scene-generator", NULL);
}

void
scm_init_dungeon_master_module ()
{
  scm_c_define_module ("dungeon-master", init_dungeon_master_module, NULL);
}

void
add_to_load_path (std::string path)
{
  // Add path to %load-path variable, needed for modules loading
  std::string exp = "(add-to-load-path \"" + path + "\")";
  scm_c_eval_string (exp.c_str ());
}

void
load_dmaster_modules ()
{
  scm_init_guile ();
  scm_init_dungeon_master_module ();

  std::set<std::string> paths = get_dmaster_paths ();
  DIR* modules_dir;
  struct dirent* module;
  std::string
    modules_path,
    module_main;

  for (const std::string &path : paths)
    {
      modules_path = path + PATH_DELIM + "modules";
      add_to_load_path (modules_path);
      modules_dir = opendir (modules_path.c_str ());
      if (modules_dir != NULL)
        {
          while (module = readdir (modules_dir))
            {
              if (strstr (module->d_name, ".scm") != NULL) {
                module_main = modules_path + PATH_DELIM + module->d_name;
                scm_primitive_load (scm_from_locale_string (module_main.c_str ()));
              }
            }
        }
      closedir (modules_dir);
    }
}
